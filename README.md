# Single Page Application Demo

Barebones examples of adding Pangea AuthN to a Single Page, React Application.

## Getting started

1. Pick which example to use:
    - Pangea Hosted Login (easiest and fastest)
    - Material-UI component-based login

### Pangea Hosted Login

Check out this branch: `AuthProvider`

### Material-UI component-based login

Check out this branch: `MUI-authprovider`