import React from "react";
import { createRoot } from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import App from "./App";
import ErrorPage from "./pages/ErrorPage";
import HomePage from "./pages/Home";
import PublicDocs from "./pages/PublicDocs";
import PrivateDocs from "./pages/PrivateDocs";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        index: true,
        element: <HomePage />,
      },
      {
        path: "docs",
        element: <PublicDocs />,
      },
      {
        path: "private/docs",
        element: <PrivateDocs />,
      },
    ],
  },
]);

const container = document.getElementById("root");
const root = createRoot(container!);

root.render(<RouterProvider router={router} />);
