import React from "react";
import { Outlet } from "react-router-dom";

export default function App() {
  return (
    <div>
      <nav>
        <ul>
          <li>
            <a href="/">Home</a>
          </li>
          <li>
            <a href="/docs">Public Docs</a>
          </li>
          <li>
            <a href="/private/docs">Private Docs</a>
          </li>
        </ul>
      </nav>
      <Outlet />
    </div>
  );
}
