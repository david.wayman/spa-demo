import React from "react";
import { useRouteError } from "react-router-dom";

export default function ErrorPage() {
  const error = useRouteError();
  console.error(error);

  // @ts-ignore need to set the type for error
  const errorStatus = error?.status;
  // @ts-ignore need to set the type for error
  const errorText = error?.statusText || error?.message;


  return (
    <div>
      <h1>{errorStatus}: {errorText}</h1>
      <p>An unexpected error has occurred.</p>
    </div>
  );
}
